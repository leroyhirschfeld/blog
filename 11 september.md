---
title: Logboek van 11-09-2017
date: 2017-09-11
---


## Design Challenge, 11-09-2017

* Wat hebben we vandaag gedaan?
Vandaag zijn we begonnen met een kleine presentatie over de stand van zaken van ons project. Het was de bedoeling dat ieder groepje dit kort samenvatte. Na de presentaties kregen we feedback over ons project. Na dat elke groep feedback kreeg, zijn we door gaan werken aan het onderzoek van ons project, om zo uiteindelijk te beginnen met het concept. Nadat we alle informatie hebben samengevat van de interviews en enquête, gaan we beginnen met het maken van ons concept op een paper prototype.

* De feedback die we hebben gekregen
Onze doelgroep ''eerstejaars studenten met een museumpas'' sloeg heel erg aan bij de docenten. Ze vonden het een leuke en interessante doelgroep die ook specifiek genoeg was. 
Daarnaast gaven de docenten aan dat we ons nu wel echt op het onderzoek moeten gaan focussen om alle informatie te gaan verzamelen zodat we ons concept kunnen uitwerken.

* Wat heb ik vandaag geleerd?
Ik heb geleerd dat onderzoek erg belangrijk is voor een project bij CMD. Omdat we ons zo erg hebben verdiept in andere dingen, terwijl we eigenlijk dat moesten besteden aan de enquête en interviewen, verliezen we hierdoor veel tijd. Als we hier eerder mee waren begonnen konden we nu al beginnen aan het concept. Dit ga ik zeker meenemen voor het volgende project en laat ik niet te lang wachten.























