---
title: Logboek van 09-14-2017
date: 09-14-2017
---



Vandaag zijn we begonnen met een werkcollege over het hoorcollege van Dinsdag. We moesten een aantal reclameafbeeldingen sorteren in de wetten van Gestalt en Ethos, Logos en Pathos. Dit moesten we daarna klassikaal presenteren. Nadat het werkcollege voorbij was, zijn we begonnen met het testen van ons paper-prototype. Van de testpersonen hebben we een aantal feedback punten gehad;

- De meeting point was vrij onduidelijk in beeld voor onze eerste tester. In haar beleving zou het fijner zijn als alles duidelijker aangegeven werd door middel van tekst en de hoeveelheid tijd van de route erbij te voegen. Voor zowel jou als speler, als de andere spelers. Dit gepaard met de afstand van de andere spelers.

- In de chatfunctie waarmee je met je matches kan chatten, zit er geen verstuur-icoon voor het verzenden van chatberichten. Dit moet nog toegevoegd worden.

- In de game zelf zit er geen knop om de vragen te beantwoorden. Dit moet ook toegevoegd worden. 

- Op dit moment zijn de spelers uit je team genummerd bij het eindscherm na het voltooien van je game. Dit moet aangepast worden naar alleen de profiel-icoontjes, met daarnaast de verdiende punten.

- Het event-icoon kan aangescherpt worden. Het is op dit moment nog niet echt duidelijk dat het te maken heeft met de pop-up events.

- Op dit moment is het match-icoon een hartje. Dit slaat meer op een soort liefdesmatch, dan een match waarmee je een vriendschappelijke relatie hebt. Misschien kunnen we dit icoon ook nog aanpassen.

- Bij het scherm van de totale puntentelling, zijn de individuele puntentelling meer in beeld, dan de verdiende punten van het gehele team. Dit zou iets meer in de picture kunnen komen.

- Een cancel-optie toevoegen voor zowel een individu als het gehele team.





















