---
title: Logboek van 07-09-2017
date: 2017-09-11
---


## Design Theory, *07-09-2017*

Vandaag gingen we verder met het hoorcollege over Design Theory, maar was het nu in principe een werkcollege. Het onderwerp ''Ontwerpen'' werd verder opgepakt in een kleine uitleg met daarbij een opdracht voor de Design Challenge groepen. We moesten een geheel ontwerpproces visualiseren op papier, vanaf de huidige situatie tot de gewenste situatie. Dit moest doorlopen worden via visuele iconen en zo weinig mogelijk tekst. We hebben gekozen voor het ontwerpproces van een t-shirt van een merk. Dit hebben we samen met ons groepje gepresenteerd.




















