---
title: Logboek van 13-09-2017
date: 2017-09-13
---




Vandaag zijn we begonnen met ons concept afmaken en hebben we om feedback gevraagd aan de docenten of ons concept überhaupt reëel is, er waren een aantal punten die we moesten veranderen, zoals; het interactieve gedeelte vergroten, mensen sneller laten matchen en het offline component vergroten. Na vele brainstormsessies hebben we het concept afgemaakt en zijn we daarna gelijk begonnen met het paper-prototype. In de namiddag hebben we het prototype afgemaakt en zijn we de dag daarna gelijk begonnen met een aantal testpersonen.

Feedback: 

 - Meer aandacht besteden aan het offline component van het concept.
 - Mensen op een makkelijkere manier laten matchen
 - Meer het gehele team erbij betrekken dan zelf al acties ondernemen.









































